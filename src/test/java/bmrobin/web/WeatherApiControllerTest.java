package bmrobin.web;

import bmrobin.service.WeatherAlertService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Map;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class WeatherApiControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WeatherAlertService weatherAlertService;

    @Test
    public void getWeatherByZip() throws Exception {
        when(weatherAlertService.getCurrentWeatherForZipcode("29640")).thenReturn(Map.of("key", "value"));
        this.mockMvc.perform(get("/zip/29640"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"key\":\"value\"}"));

        verify(weatherAlertService, times(1)).getCurrentWeatherForZipcode(eq("29640"));
    }

    @Test
    public void getWeatherByDate() throws Exception {
        this.mockMvc.perform(get("/date").param("date", ""));
    }
}
